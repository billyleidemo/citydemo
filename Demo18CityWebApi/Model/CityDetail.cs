﻿using System;
namespace Demo18CityWebApi.Model
{
    public class CityDetail
    {
		public int Id { get; set; }

        public string Detail { get; set; }

		public int CityId { get; set; }

		public CityDto City { get; set; }
       
    }
}
