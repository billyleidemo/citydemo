﻿using System;
using System.Collections.Generic;

namespace Demo18CityWebApi.Model
{
    public class User
    {
		public int Id { get; set; }
		public String Name { get; set; }
		public String Password { get; set; }
		public ICollection<UserCheckIn> UserCheckIns { get; set; }
		public String UserType { get; set; }
    }
}
