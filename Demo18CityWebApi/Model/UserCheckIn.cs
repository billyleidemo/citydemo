﻿using System;

namespace Demo18CityWebApi.Model
{
    public class UserCheckIn
    {
		public int CityId { get; set; } 
		public int UserId { get; set; }

		public CityDto CityDto { get; set; }            
		public User User{ get; set; }      

		//public string comment;
    }   
}
