﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Demo18CityWebApi.Model.DTO;
namespace Demo18CityWebApi.Model
{
	public interface ICityDataStore
    {
        //City
        IEnumerable<CityDto> GetAllCities();
        CityDto GetCityByID(int cityID);
        void AddCity(CityDto city);
        CityDto UpdateCity(int course, CityDto city);
        bool DeleteCity(int cityID);
        bool Save();

        //User
        IEnumerable<User> GetAllUsers();
		User GetUserByID(int UserID);
        void AddUser(User user);
		UserDTO Authenticate(string username, string password);

		//User Check In
		void AddCheckIn(int cityID, int userID);
		void RemoveCheckIn(int cityID, int userID);

    }

	public class CityDataStore:ICityDataStore
    {
		private CityDBContext _ctx;

		public CityDataStore(CityDBContext ctx) {
			_ctx = ctx;
		}
              
		IEnumerable<CityDto> ICityDataStore.GetAllCities()
		{
			return _ctx.Cities
				       .Include(bbbbb=>bbbbb.cityDetail)
				       .Include(city=>city.UserCheckIns)
				       .ThenInclude(b=>b.User)
				       .ToList();
		}

		public CityDto GetCityByID(int cityID)
        {
			return _ctx.Cities.Find(cityID);
        }

        public void AddCity(CityDto city)
        {
			_ctx.Cities.Add(city);
			Save();
        }

		public CityDto UpdateCity(int cityID, CityDto city)
		{
			CityDto oddCity = _ctx.Cities.Find(cityID);
			oddCity.Name = city.Name;
			oddCity.Description = city.Description;
			Save();
			return _ctx.Cities.Find(cityID);
		}
        
		bool ICityDataStore.DeleteCity(int cityID)
		{
			CityDto oddCity = _ctx.Cities.Find(cityID);
			if(oddCity != null){
				_ctx.Cities.Remove(oddCity);
			}
			return Save();

		}

		public bool Save()
		{
			return (_ctx.SaveChanges() >= 0);
		}

		public User GetUserByID(int UserID)
        {
			return _ctx.Users.Find(UserID);
        }

		public IEnumerable<User> GetAllUsers()
		{
			return _ctx.Users.ToList();
		}

		public void AddUser(User user)
		{
			_ctx.Users.Add(user);
            Save();
		}

		public void AddCheckIn(int cityID, int userID)
		{
			
			CityDto city = _ctx.Cities.Find(cityID);
			User course = _ctx.Users.Find(userID);

			var newCheckIn = new UserCheckIn { CityId = cityID,UserId = userID};
			_ctx.UserCheckIns.Add(newCheckIn);
            Save();
		}

		public void RemoveCheckIn(int cityID, int userID)
		{
			
			var checkIn = _ctx.UserCheckIns.Find(cityID,userID);
			if(checkIn != null ){
				_ctx.UserCheckIns.Remove(checkIn);
			}
			Save();
            
		}

		public UserDTO Authenticate(string username, string password)
        {
			var user = _ctx.Users.SingleOrDefault(x => x.Name == username && x.Password == password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
			var key = Encoding.ASCII.GetBytes("ThisissercertKeyforFullStackWebAPIDEMO");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
					new Claim(ClaimTypes.Role, user.UserType)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

			var userDTO = new UserDTO();
			userDTO.Name = user.Name;
			userDTO.Token = tokenHandler.WriteToken(token);
           
			return userDTO;
        }
	}
}
