﻿using System;
namespace Demo18CityWebApi.Model.DTO
{
    public class CityListDTO
    {
		public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

		public CityListDTO(CityDto dto)
        {
			this.Id = dto.Id;
			this.Name = dto.Name;
			this.Description = dto.Description;
        }
    }
}
