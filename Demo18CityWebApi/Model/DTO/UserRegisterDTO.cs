﻿using System;
namespace Demo18CityWebApi.Model.DTO
{
    public class UserRegisterDTO
    {
		public string Name { get; set; }
        public string Password { get; set; }
		public string UserType { get; set; }
    }
}
