﻿using System;
namespace Demo18CityWebApi.Model.DTO
{
    public class UserDTO
    {
		public string Name { get; set; }
        public string Token { get; set; }
    }
}
