﻿using System;
using System.Collections.Generic;

namespace Demo18CityWebApi.Model
{
    public class CityDto
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int NumberOfPointInterest { get; set; }
       
		public CityDetail cityDetail { get; set; }
		public ICollection<CityPoint> CityPoints { get; set; }

		public ICollection<UserCheckIn> UserCheckIns { get; set; }      
    }
}
