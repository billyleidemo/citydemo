﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Demo18CityWebApi.Model
{
	public class CityDBContext:DbContext
    {      
		
		public DbSet<CityDto> Cities { get; set; }
		public DbSet<CityDetail> CityDetail { get; set; }
		public DbSet<CityPoint> CityPoint { get; set; }
		public DbSet<User> Users { get; set; }

		public DbSet<UserCheckIn> UserCheckIns { get; set; }
       
		public CityDBContext(DbContextOptions<CityDBContext> options) : base(options)
		{
			//Database.EnsureCreated();
			//Database.Migrate();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder){
			
			//City
			modelBuilder.Entity<CityDto>().Property(a => a.Id).ValueGeneratedOnAdd();
			modelBuilder.Entity<CityDto>().HasKey(a=>a.Id);         
            //City Detail 1 to 1
			modelBuilder.Entity<CityDetail>()
            .Property(a => a.Id).ValueGeneratedOnAdd();

			modelBuilder.Entity<CityDetail>()
                        .HasKey(a => a.Id);
           
			modelBuilder.Entity<CityDto>()
			            .HasOne(city => city.cityDetail)
			            .WithOne(citydetail => citydetail.City)
			            .HasForeignKey<CityDetail>(citydetail => citydetail.CityId);
			
			//City Detail 1 to Many         
			modelBuilder.Entity<CityPoint>()
            .Property(a => a.Id).ValueGeneratedOnAdd();

			modelBuilder.Entity<CityPoint>()
                        .HasKey(a => a.Id);
                        
			modelBuilder.Entity<CityDto>()
			            .HasMany(city => city.CityPoints)
			            .WithOne(abc => abc.City)
			            .HasForeignKey(citydetail => citydetail.CityId); 
            
            //User
			modelBuilder.Entity<User>()
            .Property(a => a.Id).ValueGeneratedOnAdd();

			modelBuilder.Entity<User>()
                        .HasKey(a => a.Id);

			//City Detail Many to Many         
			modelBuilder.Entity<UserCheckIn>()
			            .HasKey(test=> new{test.CityId,test.UserId}); //Remeber the Order

            
  			modelBuilder.Entity<UserCheckIn>()
			            .HasOne(test => test.CityDto)
			            .WithMany(b => b.UserCheckIns)
			            .HasForeignKey(bc => bc.CityId);

			modelBuilder.Entity<UserCheckIn>()
			            .HasOne(bc => bc.User)
			            .WithMany(b => b.UserCheckIns)
			            .HasForeignKey(bc => bc.UserId);


		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
			var connectionString = "server=localhost;userid=root;pwd=password;port=3306;database=lms_demo;sslmode=none";
			//var connectionString = "server=13.54.17.147;userid=lms_demo;pwd=password;port=3306;database=lms_demo;sslmode=none";         
			optionsBuilder.UseMySQL(connectionString);
            base.OnConfiguring(optionsBuilder);
		}

    }
}
