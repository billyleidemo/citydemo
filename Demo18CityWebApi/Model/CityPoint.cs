﻿using System;
namespace Demo18CityWebApi.Model
{
    public class CityPoint
    {
		public int Id { get; set; }

        public string PointName { get; set; }

		public int CityId { get; set; }

        public CityDto City { get; set; }
    }
}
