﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Demo18CityWebApi.Model;
using Demo18CityWebApi.Model.DTO;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Demo18CityWebApi.Controllers
{
    [Route("api/CheckIn")]
    public class CheckInController : Controller
    {

		private ICityDataStore _dataStore;

		public CheckInController(ICityDataStore dataStore)
		{
			_dataStore = dataStore;
		}
        [HttpPost]
		public void Post([FromBody] CheckInDTO checkIn)
        {
			_dataStore.AddCheckIn(checkIn.CityID, checkIn.UserID);
        }
  

        [HttpDelete("{id}")]
		public void Delete([FromBody] CheckInDTO checkIn)
        {
			_dataStore.RemoveCheckIn(checkIn.CityID, checkIn.UserID);         
        }
    }
}
