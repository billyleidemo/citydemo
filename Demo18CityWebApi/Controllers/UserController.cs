﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Demo18CityWebApi.Model;
using Demo18CityWebApi.Model.DTO;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Demo18CityWebApi.Controllers
{
	[Authorize]
    [Route("api/user")]
    public class UserController : Controller
    {
		private ICityDataStore _dataStore;

		public UserController(ICityDataStore dataStore)
        {
            _dataStore = dataStore;
        
		}
		[Authorize(Roles = "Admin")]
		[HttpGet("/admin")]
		public IActionResult AdminGet()
        {
			var result = _dataStore.GetAllUsers();
			return Ok(result);
        }

        /*
		[HttpGet]
        public IActionResult Get()
        {
			var userName = this.User.Identity.Name;
            var result = _dataStore.GetAllUsers();
            return Ok(result);
        }*/
		[HttpGet]
        public IActionResult Get()
        {
            var userID = this.User.Identity.Name;
			var result = _dataStore.GetUserByID(int.Parse(userID));
            return Ok(result);
        }
       

		[AllowAnonymous]
		[HttpPost("/auth")]
		public IActionResult Authenticate([FromBody]LoginDTO loginDTO)
        {
			var result = _dataStore.Authenticate(loginDTO.LoginName, loginDTO.Password);
			if(result != null) {
				return Ok(result);
			} else {
				return Forbid();
			}
        }
    }
}
